// index.js

/*

HTTP METHODS

	GET 	Retrieves resources
	POST	Sends data for creating a resource
	PUT 	Sends data for updating a resource
	DELETE 	Deletes a specified resource

*/

/*

	console.log(request.url) // differentiate requests via endpoints
	console.log(request.method) // differentiate requests with http methods

	HTTP Requests are differentiated not only via endpoints but also with their methods.

HTTP METHODS
		-simply tells the server what action it must take or what kind of response is needed for the request.
		- with HTTP methods we can create routes with the same endpoint but with different methods.

SENDING JSON 

	When sending a JSON format data as response, change the Content-Type of the response to application/json

	Stringify

RECEIVE INPUT FROM THE CLIENT

	Route to add a new course (in our example), we have to receive an input from the client. To receive the request body or the input from the request, 2 steps are done in NodeJS.

	requestBody will be a placeholder to contain the data (request body) passed from the client

	1st - data step
		- will read the incoming stream of data from client and process it so we can save it in the requestBody variable

		we have completely received the data from the client and thus requestBody now contains our input

		initially, requestBody is in JSON format. We cannot add this to our courses array because it's a string. So, we have to update requestBody variable with a parsed version of the received JSON format data.

		

*/

let http = require("http");

let courses = [

	{
		name: "Python 101",
		description: "Learn Python",
		price: 25000
	},
	{
		name: "ReactJS 101",
		description: "Learn React",
		price: 35000
	},
	{
		name: "ExpressJS 101",
		description: "Learn ExpressJS",
		price: 28000
	}

];


http.createServer(function(request,response){

	console.log(request.url);
	console.log(request.method);

	if(request.url === "/" && request.method === "GET"){

		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end("Hello from our 4th server. GET method request.");

	} else if (request.url === "/" && request.method === "POST"){

		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end("This is the / endpoint. POST method request.");

	} else if (request.url === "/" && request.method === "PUT"){

		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end("This is the / endpoint. PUT method request.");

	} else if (request.url === "/" && request.method === "DELETE"){

		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end("This is the / endpoint. DELETE method request.");

	} else if (request.url === "/courses" && request.method === "GET"){

		response.writeHead(200,{'Content-Type':'application/json'});
		//response.end("This is a response to a GET method request for the /courses endpoint.");
		response.end(JSON.stringify(courses));

	} else if (request.url === "/courses" && request.method === "POST"){

		let requestBody = "";

		request.on('data',function(data){

			// console.log(data);
			requestBody += data;

		})

		request.on('end',function(){

			//console.log(requestBody);
			requestBody = JSON.parse(requestBody);
			// console.log(requestBody); 
			courses.push(requestBody);
			console.log(courses);

			response.writeHead(200,{'Content-Type':'application/json'});
			response.end(JSON.stringify(courses));
		})

		// response.writeHead(200,{'Content-Type':'text/plain'});
		// response.end("This is a response to a POST method request for the /courses endpoint.");
	};



}).listen(4000);

console.log("Server running at localhost:4000");


